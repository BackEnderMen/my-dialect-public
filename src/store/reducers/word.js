import { ActionType } from 'redux-promise-middleware';
import { handleActions } from 'redux-actions';
import {
  getDialectWords, selectDialectWord, createWord, getWord, clearWord,
} from '../actions';

const reducer = handleActions(
  {
    [getDialectWords.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({ ...state, dialectWords: action.payload.words }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [selectDialectWord.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({ ...state, selectedWord: action.payload }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [createWord.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        dialectWords: [...state.dialectWords, action.payload.word],
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [getWord.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        word: action.payload,
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [clearWord.toString()]: {
      [ActionType.Fulfilled]: () => ({
        dialectWords: [],
        officialWords: [],
        word: null,
        selectedWord: null,
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
  },
  {
    dialectWords: [],
    officialWords: [],
    word: null,
    selectedWord: null,
  },
);

export default reducer;
