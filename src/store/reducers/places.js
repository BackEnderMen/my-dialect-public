import { ActionType } from 'redux-promise-middleware';
import { handleActions } from 'redux-actions';
import {
  setCities,
  getCountries,
  selectCountry,
  selectDistrict,
  selectRegion, getDialectWordsByTranscrition,
} from '../actions';


const reducer = handleActions(
  {
    [setCities.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({ ...state, cities: { ...state.cities, ...action.payload } }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [getCountries.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        countries: action.payload.countries,
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [selectCountry.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        districts: action.payload,
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [selectDistrict.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        regions: action.payload,
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [selectRegion.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        regions: state.regions.map((r) => {
          if (action.payload.id === r.id) r.selected = true;
          else r.selected = false;
          return r;
        }),
        cities: { ...state.cities, ...action.payload.cities },
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [getDialectWordsByTranscrition.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        cities: action.payload,
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
  },
  {
    countries: [],
    districts: [],
    regions: [],
    cities: {},
    scope: {},
  },
);

export default reducer;
