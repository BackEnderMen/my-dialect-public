import { ActionType } from 'redux-promise-middleware';
import { handleActions } from 'redux-actions';
import {
  getCategoryByLocale,
  addCategory,
  removeCategory,
  updateCategory,
} from '../actions';

const reducer = handleActions(
  {
    [getCategoryByLocale.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({ ...state, ...action.payload }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [addCategory.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({ ...state, categories: [...state.categories, action.payload] }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [removeCategory.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        categories: state.categories.filter((c) => c.id !== action.payload),
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
    [updateCategory.toString()]: {
      [ActionType.Fulfilled]: (state, action) => ({
        ...state,
        categories: state.categories.map((c) => {
          if (c.id === action.payload.id) return action.payload.category;
          return c;
        }),
      }),
      [ActionType.Rejected]: (state, action) => ({ ...state, ...action.payload }),
    },
  },
  {
    categories: [],
  },
);

export default reducer;
