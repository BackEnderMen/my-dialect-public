import { combineReducers } from 'redux';
import user from './user';
import category from './category';
import word from './word';
import places from './places';

const reducers = combineReducers({
  category,
  user,
  word,
  places,
});

export default reducers;
