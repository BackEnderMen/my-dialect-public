import { createAction } from 'redux-actions';
import agent from '../../agent';

export const getCategoryByLocale = createAction(
  'CATEGORY/FETCH/LIST',
  async (locale) => {
    const categories = await agent.Category.getList(locale);
    return { categories };
  },
);

export const addCategory = createAction('CATEGORY/ADD', async (data) => {
  const res = await agent.Category.add(data);
  return res;
});

export const removeCategory = createAction('CATEGORY/REMOVE', async (id) => {
  await agent.Category.remove(id);
  return id;
});


export const updateCategory = createAction('CATEGORY/UPDATE', async (id, params) => {
  const category = await agent.Category.update(id, params);
  return { id, category };
});
