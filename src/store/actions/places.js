import { createAction } from 'redux-actions';
import { omit } from 'lodash';
import agent from '../../agent';

export const setCities = createAction('PLACES/CITIES/SET', async (cities) => {
  const normilizedCities = cities.reduce((arr, curr) => {
    arr[curr.id] = curr;
    return arr;
  }, {});

  return normilizedCities;
});

export const getCountries = createAction('PLACES/COUNTRIES/GET', async () => {
  const res = await agent.Places.getCountries();
  const countries = res.map((c) => omit(c, 'districts'));
  return { countries };
});

export const selectCountry = createAction(
  'PLACES/COUNTRIES/SELECT',
  async (id) => {
    const country = await agent.Places.getCountry(id);
    return country.districts;
  },
);

export const selectDistrict = createAction(
  'PLACES/DISTRICTS/SELECT',
  async (id) => {
    const res = await agent.Places.getDistrict(id);
    return res.regions;
  },
);

export const selectRegion = createAction(
  'PLACES/REGIONS/SELECT',
  async (id) => {
    const res = await agent.Places.getRegion(id);
    console.log('setCities -> res', res);
    return {
      id,
      cities: res.cities.reduce((arr, curr) => {
        arr[curr.id] = curr;
        return arr;
      }, {}),
    };
  },
);

export const getDialectWordsByTranscrition = createAction(
  'PLACES/CITIES/GET_BY_TRANSCRIPTION',
  async (name, transcription) => {
    const res = await agent.Places.getCitiesTranscription(name, transcription);
    return res;
  },
);
