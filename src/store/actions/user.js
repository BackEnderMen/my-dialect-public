import { createAction } from 'redux-actions';
import agent from '../../agent';

const { Auth } = agent;

export const authLogin = createAction('AUTH/LOGIN', async (email, password) => {
  const auth = await Auth.login(email, password);
  Auth.set(auth.jwt);
  localStorage.setItem('token', auth.jwt);
  return { token: auth.jwt };
});

export const authLogout = createAction('AUTH/LOGOUT', async () => {
  Auth.set('');
  return { user: null, token: null };
});

export const getUser = createAction('USER/GET', async () => {
  const user = await agent.User.get();
  return { user };
});

export const authRegister = createAction('AUTH/REGISTER', async (email, password) => {
  const user = await agent.Auth.register(email, password);
  return { user };
});
