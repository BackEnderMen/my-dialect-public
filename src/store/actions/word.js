/* eslint-disable  array-callback-return */
import { createAction } from 'redux-actions';
import agent from '../../agent';

export const getDialectWords = createAction(
  'WORD/GET/SEARCH',
  async (search) => {
    const wordsResult = await agent.Words.getWords(search);

    return wordsResult;
  },
);

export const getWord = createAction('WORD/GET', async (name, transcription) => {
  const wordsResult = await agent.Words.getWords(name);
  wordsResult.words.filter(
    (w) => w.name === name && w.transcription === transcription,
  );

  const word = wordsResult.words[0];

  const mp = new Map();
  const ans = wordsResult.words.map((w) => w.category_values);

  ans.map((a) => Object.entries(a).forEach(([key, value]) => {
    mp.get(`${key} | ${value}`) === undefined
      ? mp.set(`${key} | ${value}`, 1)
      : mp.set(`${key} | ${value}`, mp.get(`${key} | ${value}`) + 1);
  }));

  const res = [...mp.entries()].reduce((acc, [k, v]) => {
    acc.has(v) ? acc.set(v, acc.get(v).concat(k)) : acc.set(v, [k]);
    return acc;
  }, new Map());

  const mapAsc = new Map([...res.entries()].sort((x, y) => (x > y ? x : y)));
  let i = 0;
  const categLast = {};
  mapAsc.forEach((value) => {
    if (typeof value === 'string' && i <= 5) {
      const [categKey, categValue] = value.split(' | ');
      categLast[categKey] = categValue;
      i++;
    } else if (i <= 5) {
      value.map((v) => {
        const [categKey, categValue] = v.split(' | ');
        if (i <= 5) categLast[categKey] = categValue;
        i++;
      });
    }
  });

  word.cities = wordsResult.cities.map((city) => {
    const found = wordsResult.words.find((w) => city.id === w.city_id);
    if (found && found.category_values) {
      city.category_values = found.category_values;
    }
    return city;
  });
  word.category_values = categLast;

  return word;
});

export const selectDialectWord = createAction(
  'WORD/SELECT',
  async (word) => word,
);

export const clearWord = createAction(
  'WORD/CLEAR',
);

export const createWord = createAction('WORD/CREATE', async (data) => {
  const word = await agent.Words.saveWordPair(data);
  return { word };
});
