import agent from '../agent';
// import decode from 'jwt-decode';

export default function preloadState() {
  let user;

  try {
    const token = localStorage.getItem('token');
    const locale = localStorage.getItem('locale');
    user = {
      token: token || '',
      locale: locale || 'ua',
    };
  } catch (e) {}

  if (user) {
    agent.Auth.set(user.token || '');
  }
  return { user };
}
