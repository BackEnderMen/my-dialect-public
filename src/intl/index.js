import hu from './hu.json';
import ua from './ua.json';
import en from './en.json';

export const messages = { hu, ua, en };

export const DEFAULT_LOCALE = 'en';

export default function intlMessages(locale) {
  return messages[locale] || messages[DEFAULT_LOCALE] || '<no value>';
}
