import React from 'react';
import PropTypes from 'prop-types';
import styles from './Input.module.scss';

export const Input = React.forwardRef(({
  onChange = () => {},
  value,
  label,
  placeholder,
  classNames = {},
  field,
  ...props
}, ref) => {
  const handleChange = (e) => onChange(e.target.value);

  return (
    <div className={[styles.root, classNames.root].join(' ')}>
      <label
        htmlFor="input"
        className={[styles.label, classNames.label].join(' ')}
        data-testid="test-label"
      >
        {label}
      </label>
      <input
        ref={ref}
        name="input"
        type="text"
        placeholder={placeholder}
        className={[styles.input, classNames.input].join(' ')}
        value={value}
        onChange={handleChange}
        {...props}
        {...field}
      />
    </div>
  );
});

Input.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  classNames: PropTypes.exact({
    root: PropTypes.string,
    label: PropTypes.string,
    input: PropTypes.string,
  }),
  field: PropTypes.any,
};

Input.defaultProps = {
  onChange: () => {},
  value: '',
  label: '',
  placeholder: '',
  classNames: {
    root: '',
    label: '',
    input: '',
  },
  field: null,
};
