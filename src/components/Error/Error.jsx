import React from 'react';
import PropTypes from 'prop-types';
import styles from './Error.module.scss';

export const ErrorBase = ({ value, children }) => (
  <span className={styles.error}>{value || children }</span>
);

ErrorBase.propTypes = {
  value: PropTypes.string,
};

ErrorBase.defaultProps = {
  value: '',
};
