import React from 'react';
import PropTypes from 'prop-types';
import styles from './Typography.module.scss';

export const Typography = ({
  variant,
  className,
  children,
  ...props
}) => React.createElement(
  variant,
  {
    className: [className, styles.typography, styles[variant]].join(' '),
    ...props,
  },
  children,
);

Typography.propTypes = {
  variant: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'p'])
    .isRequired,
  className: PropTypes.string,
};

Typography.defaultProps = {
  className: '',
};
