import React from 'react';
import PropTypes from 'prop-types';
import styles from './Audio.module.scss';

export const Audio = ({ src }) => (
  <audio src={src} controls className={styles.audio} />
);

Audio.propTypes = {
  src: PropTypes.string.isRequired,
};
