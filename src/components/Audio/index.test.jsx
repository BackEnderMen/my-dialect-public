import React from 'react';
import { render } from '@testing-library/react';
import Audio from './index';
import TestAudio from './Audio.test.mp3';

describe('<Audio />', () => {
  it('renders the component', () => {
    const { container } = render(<Audio src={TestAudio} />);

    expect(container).toMatchSnapshot();
  });
});
