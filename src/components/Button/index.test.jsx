import React from 'react';
import { render, fireEvent } from '@testing-library/react';

import Button from './index';

describe('<Button />', () => {
  const handleClick = jest.fn();

  describe('when is loading', () => {
    it('renders the component', () => {
      const { container } = render(<Button loading value="some value" />);

      expect(container).toMatchSnapshot();
    });
  });

  describe('when is not loading', () => {
    it('renders the component', () => {
      const { container } = render(
        <Button loading={false} value="some value" />,
      );

      expect(container).toMatchSnapshot();
    });

    it('calls handleClick', () => {
      /* eslint-disable-next-line react/no-children-prop */
      const { getByText } = render(
        <Button onClick={handleClick} value="some value">
          Button
        </Button>,
      );
      fireEvent.click(getByText('Button'));

      expect(handleClick).toHaveBeenCalled();
    });

    it('has setted className', () => {
      /* eslint-disable-next-line react/no-children-prop */
      const { getByText } = render(
        <Button className="test" value="some value">
          Button
        </Button>,
      );

      expect(getByText('Button')).toHaveClass('test');
    });
  });
});
