import React from 'react';
import { render } from '@testing-library/react';

import MediaButton from './index';

describe('<MediaButton />', () => {
  it('renders the component', () => {
    const { container } = render(<MediaButton label="label" />);

    expect(container).toMatchSnapshot();
  });

  it('renders label', () => {
    const { getByText } = render(<MediaButton label="label" />);

    const label = getByText('label');
    expect(label).toBeInTheDocument();
  });

  it('founded by testId', () => {
    const { getByTestId } = render(<MediaButton label="label" />);

    const linkElement = getByTestId('test-label');
    expect(linkElement).toHaveTextContent('label');
  });
});
