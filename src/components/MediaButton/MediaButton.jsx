import React from 'react';
import PropTypes from 'prop-types';

import classNames from '../../helpers/classNames';
import Input from '../Input';
import styles from './MediaButton.module.scss';

export const MediaButton = ({
  mediaType,
  className,
  size,
  variant,
  ...props
}) => {
  const styleClasses = () => (
    classNames(
      styles.root,
      styles[size],
      className || styles[variant],
    )
  );

  return (
    <Input
      type="file"
      accept={mediaType}
      className={styleClasses()}
      {...props}
    />
  );
};

MediaButton.propTypes = {
  mediaType: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.string,
  variant: PropTypes.string,
};

MediaButton.defaultProps = {
  mediaType: 'jpg',
  className: null,
  size: 'large',
  variant: 'primary',
};
