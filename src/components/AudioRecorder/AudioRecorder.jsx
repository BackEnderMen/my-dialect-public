import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { ReactMic } from 'react-mic';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMicrophone } from '@fortawesome/free-solid-svg-icons';
import { useIntl } from 'react-intl';
import { Button } from '../Button/Button';
import styles from './AudioRecorder.module.scss';

export const AudioRecorder = ({ onRecord }) => {
  const { formatMessage: f } = useIntl();

  const successText = f({ id: 'success' });
  const recordText = f({ id: 'record' });

  const [record, setRecord] = useState(false);
  const [activeType, setActiveType] = useState(null);
  const [text, setText] = useState(recordText);

  const handleStop = (recordBlob) => {
    onRecord(recordBlob);
  };

  const handleData = (data) => {
    console.log('handleData -> data', data);
  };

  const handleRecord = () => {
    if (record) {
      setActiveType(styles.successButton);
      setText(successText);
      setRecord(false);

      setTimeout(() => {
        setActiveType(null);
        setText(recordText);
      }, 3000);
    } else {
      setActiveType(styles.stopButton);
      setRecord(true);
    }
  };

  return (
    <div>
      <ReactMic
        record={record}
        className="sound-wave"
        onStop={handleStop}
        onData={handleData}
        strokeColor="#34558b"
        backgroundColor="white"
      />
      <Button
        onClick={text !== successText ? handleRecord : null}
        className={activeType}
      >
        {record ? <FontAwesomeIcon icon={faMicrophone} /> : text}
      </Button>
    </div>
  );
};

AudioRecorder.propTypes = {
  onRecord: PropTypes.func.isRequired,
};
