import React from 'react';
import { render } from '@testing-library/react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Checkbox from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<Checkbox/>', () => {
  it('renders the component', () => {
    const { container } = render(<Checkbox />);
    expect(container).toMatchSnapshot();
  });

  describe('checkbox value', () => {
    it('expected false', () => {
      const { getByTestId } = render(<Checkbox />);
      const checkbox = getByTestId('checkbox');
      expect(checkbox.checked).toEqual(false);
    });
    it('expected true', () => {
      const { getByTestId } = render(<Checkbox value />);
      const checkbox = getByTestId('checkbox');
      expect(checkbox.checked).toEqual(true);
    });
  });

  it('on a label click', () => {
    const handleClick = jest.fn();
    const wrapper = mount(<Checkbox onChange={handleClick} />);
    wrapper.find('label').simulate('click');
    expect(handleClick).toBeCalled();
  });

  it('has setted className', () => {
    const { getByTestId } = render(<Checkbox className="test" />);
    const div = getByTestId('div');
    expect(div).toHaveClass('test');
  });
});
