import React, { useState, useEffect, useLayoutEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './CheckBox.module.scss';
import generateId from '../../helpers/generateId';

export const CheckBox = (props) => {
  const {
    onChange, value, className, label,
  } = props;
  const [id, setId] = useState(null);
  const handleChange = (e) => {
    onChange(e.target.checked);
  };

  useLayoutEffect(() => {
    setId(generateId());
  }, []);
  useEffect(() => {
    onChange(value);
  }, []);
  return (
    <div className={[styles.checkbox, className].join(' ')} data-testid="div">
      <span>
        <input
          type="checkbox"
          defaultChecked={value}
          onChange={handleChange}
          id={`checkbox${id}`}
          data-testid="checkbox"
        />
        <label htmlFor={`checkbox${id}`}>{label}</label>
      </span>
    </div>
  );
};

CheckBox.propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.bool,
  className: PropTypes.string,
  label: PropTypes.string,
};

CheckBox.defaultProps = {
  onChange: () => {},
  value: false,
  className: '',
  label: '',
};
