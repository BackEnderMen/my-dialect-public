import React from 'react';
import PropTypes from 'prop-types';
import { useIntl } from 'react-intl';
import styles from './Popup.module.scss';
import Button from '../Button';

export const Popup = ({
  children,
  onSubmit,
  onExit,
  visible,
  positiveAnswer,
  negativeAnswer,
}) => {
  const { formatMessage: f } = useIntl();
  const handleSubmit = () => {
    onSubmit();
  };
  const handleExit = () => {
    onExit();
  };

  return visible ? (
    <div className={[styles.bg_modal].join(' ')}>
      <div className={[styles.modal_content].join(' ')}>
        <div className={styles.body}>{children}</div>
        <div className={[styles.buttons].join(' ')}>
          <Button
            size="small"
            value={positiveAnswer || f({ id: 'yes' })}
            variant="primary"
            onClick={handleSubmit}
            className={[styles.btnSubmit].join(' ')}
          />
          {onExit ? (
            <Button
              size="small"
              value={negativeAnswer || f({ id: 'no' })}
              variant="primary"
              onClick={handleExit}
              className={[styles.btnExit].join(' ')}
            />
          ) : null}
        </div>
      </div>
    </div>
  ) : null;
};

Popup.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onExit: PropTypes.func,
  children: PropTypes.any,
  visible: PropTypes.bool,
  positiveAnswer: PropTypes.string,
  negativeAnswer: PropTypes.string,
};

Popup.defaultProps = {
  onExit: () => {},
  children: null,
  visible: false,
  positiveAnswer: '',
  negativeAnswer: '',
};
