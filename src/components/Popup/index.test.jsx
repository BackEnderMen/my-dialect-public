import React from 'react';
import { render } from '@testing-library/react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { IntlProvider } from 'react-intl';
import Popup from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<Popup />', () => {
  describe('renders the component', () => {
    it('when visible=false', () => {
      const { container } = render(
        <IntlProvider locale="en">
          <Popup />
        </IntlProvider>,
      );
      expect(container).toMatchSnapshot();
    });
    it('when visible=true', () => {
      const { container } = render(
        <IntlProvider locale="en">
          <Popup visible />
        </IntlProvider>,
      );
      expect(container).toMatchSnapshot();
    });
    it('when second button is not visible', () => {
      const { container } = render(
        <IntlProvider locale="en">
          <Popup visible onExit={null} />
        </IntlProvider>,
      );
      expect(container).toMatchSnapshot();
    });
  });

  it('has setted text for children elem', () => {
    const { getByText } = render(
      <IntlProvider locale="en">
        <Popup visible>Text</Popup>
      </IntlProvider>,
    );
    expect(getByText('Text')).toHaveTextContent('Text');
  });

  describe('call for function', () => {
    const handleClickSub = jest.fn();
    const handleClickExit = jest.fn();

    const wrapper = mount(
      <IntlProvider locale="en">
        <Popup
          visible
          onExit={handleClickExit}
          onSubmit={handleClickSub}
        />
      </IntlProvider>,
    );
    it('onSubmit', () => {
      wrapper.find('button.btnSubmit').simulate('click');
      expect(handleClickSub).toBeCalled();
    });
    it('onExit', () => {
      wrapper.find('button.btnExit').simulate('click');
      expect(handleClickExit).toBeCalled();
    });
  });
});
