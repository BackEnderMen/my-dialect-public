import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPlus,
  faMinus,
  faTimes,
  faMapMarker,
} from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import styles from './Toolbar.module.scss';

export const Toolbar = ({ checkZoom }) => (
  <div className={styles.toolbar}>
    <a
      onClick={() => checkZoom('+')}
      role="menuitem"
      tabIndex="0"
      onKeyDown={() => {}}
    >
      <FontAwesomeIcon icon={faPlus} />
    </a>
    <a
      onClick={() => checkZoom('-')}
      role="menuitem"
      tabIndex="0"
      onKeyDown={() => {}}
    >
      <FontAwesomeIcon icon={faMinus} />
    </a>
    <a
      role="menuitem"
      tabIndex="0"
    >
      <FontAwesomeIcon icon={faTimes} />
    </a>
    <a
      role="menuitem"
      tabIndex="0"
    >
      <FontAwesomeIcon icon={faMapMarker} />
    </a>
  </div>
);

Toolbar.propTypes = {
  checkZoom: PropTypes.func.isRequired,
};
