/* eslint-disable jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions,
 jsx-a11y/anchor-is-valid, react/prop-types, no-shadow, array-callback-return */

import React, { useState, useRef } from 'react';
import {
  Map, Marker, GeoJSON, TileLayer,
} from 'react-leaflet';
import MarkerClusterGroup from 'react-leaflet-markercluster';

import styles from './Map.module.scss';
import { MapPopup } from './MapPopup/MapPopup';
import Toolbar from './Toolbar';

import Zakarpattia from './GeoJSON/districts/Zakarpattia.json';
import Uzhgorodskyi from './GeoJSON/regions/Uzhhorodskyi.json';
import Svalyavskyi from './GeoJSON/regions/Svalyavskyi.json';
import Rakhivskyi from './GeoJSON/regions/Rakhivskyi.json';
import Perechynskyi from './GeoJSON/regions/Perechynskyi.json';
import Muckachivskyi from './GeoJSON/regions/Muckachivskyi.json';
import Khustsky from './GeoJSON/regions/Khustsky.json';
import Tiachivskyi from './GeoJSON/regions/Tiachivskyi.json';
import Mizhhirya from './GeoJSON/regions/Mizhhirya.json';
import Velykyi_Bereznyi from './GeoJSON/regions/Velykyi_Bereznyi.json';
import Volovets from './GeoJSON/regions/Volovets.json';
import Vynohradiv from './GeoJSON/regions/Vynohradiv.json';
import Irshava from './GeoJSON/regions/Irshava.json';
import Beregovsky from './GeoJSON/regions/Beregovsky.json';

export const MapComponent = (props) => {
  const {
    words = [], cities = [], word, selectDialectWord,
  } = props;
  const position = [50.4510484, 30.5215503];
  const [zoom, setZoom] = useState(5.3);
  const mapRef = useRef();
  console.log('MapComponent -> mapRef', mapRef);

  /* eslint-disable no-unused-vars */
  const getRandomColor = () => {
    const color = `hsl(${Math.random() * 360}, 100%, 75%)`;
    return color;
    // return `#${Math.floor(100000 + Math.random() * 900000)}`;
  };

  const checkZoom = (operation) => {
    if (operation === '+') {
      if (zoom + 1 > 10) setZoom(10);
      else setZoom(zoom + 1);
    } else if (operation === '-') {
      if (zoom - 1 < 0.1) setZoom(0.1);
      else setZoom(zoom - 1);
    }
  };

  const handleSelectWord = (word) => selectDialectWord(word);

  return (
    <>
      <Map
        center={position}
        zoom={zoom}
        maxZoom={10}
        animate
        className={styles.map}
        ref={mapRef}
        zoomControl={false}
        onViewportChange={(event) => setZoom(event.zoom)}
      >
        <Toolbar checkZoom={checkZoom} />
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        />
        {zoom < 9 ? (
          <GeoJSON key="geojson1" data={[Zakarpattia]} />
        ) : (
          <GeoJSON
            key="geojson12"
            data={[
              Uzhgorodskyi,
              Svalyavskyi,
              Rakhivskyi,
              Perechynskyi,
              Muckachivskyi,
              Khustsky,
              Tiachivskyi,
              Mizhhirya,
              Velykyi_Bereznyi,
              Volovets,
              Vynohradiv,
              Irshava,
              Beregovsky,
            ]}
          />
        )}

        <MarkerClusterGroup>
          {word && <MapPopup word={word} key={word.id} />}
          {words.map((word) => {
            const city = cities[word.city_id];
            if (city) {
              return (
                <Marker
                  position={[city?.longitude, city?.latitude]}
                  onClick={() => handleSelectWord({ ...word, city })}
                  key={word.id}
                />
              );
            }
            return null;
          })}
        </MarkerClusterGroup>
      </Map>
    </>
  );
};
