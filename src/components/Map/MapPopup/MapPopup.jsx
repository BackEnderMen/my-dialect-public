import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { Popup } from 'react-leaflet';
import { useHistory } from 'react-router-dom';
import styles from './MapPopup.module.scss';
import { Button } from '../../Button/Button';
import { Typography } from '../../Typography/Typography';
import classNames from '../../../helpers/classNames';
import '../MapStyle.scss';


export const MapPopup = ({ setShowModal, word }) => {
  const {
    official_word, transcription, city, name, category_values,
  } = word;
  const popupRef = useRef();

  const history = useHistory();

  useEffect(() => () => {
    setShowModal(false);
  }, []);

  const styleClasses = () => (
    classNames(
      styles.popup,
    )
  );

  const handleRedirect = () => {
    history.push(`/vocabulary/${name}/${transcription}/`);
  };

  return (
    <Popup key="leaflet-popup" className={styleClasses()} ref={popupRef} position={[city?.longitude, city?.latitude]}>
      <header>
        <Typography variant="h4">
          {name}
          {' '}
          [
          {transcription}
          ]
        </Typography>
        <Typography variant="p">Слово-діалект</Typography>
      </header>

      <article>
        <Typography variant="h4">{official_word?.name}</Typography>
        <Typography variant="p">Літературне слово</Typography>

        <Typography variant="h4">{city?.name}</Typography>
        <Typography variant="p">Місто</Typography>

        <div className={styles.category_values}>
          {
            Object.entries(category_values).map(([key, value]) => (
              <>
                <Typography variant="h4">{key}</Typography>
                <Typography variant="p">{value}</Typography>
              </>
            ))
          }
        </div>

        <Button
          className={styles.modalBtn}
          value="Перейти"
          variant="primary"
          size="medium"
          onClick={handleRedirect}
        />
      </article>
    </Popup>
  );
};

MapPopup.propTypes = {
  setShowModal: PropTypes.func,
  word: PropTypes.shape({
    name: PropTypes.string.isRequired,
    category_values: PropTypes.object.isRequired,
    city: PropTypes.shape({
      name: PropTypes.string.isRequired,
      longitude: PropTypes.string.isRequired,
      latitude: PropTypes.string.isRequired,
    }).isRequired,
    transcription: PropTypes.string.isRequired,
    official_word: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  }).isRequired,
};

MapPopup.defaultProps = {
  setShowModal: () => {},
};
