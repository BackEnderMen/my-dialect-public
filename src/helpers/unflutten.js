/* eslint-disable  no-restricted-syntax, guard-for-in, brace-style */

export const unflutten = (arr) => {
  const tree = [];
  const mappedArr = {};
  let arrElem;
  let mappedElem;

  for (let i = 0, len = arr.length; i < len; i += 1) {
    arrElem = arr[i];
    mappedArr[arrElem.id] = arrElem;
    mappedArr[arrElem.id].children = [];
  }

  for (const id in mappedArr) {
    mappedElem = mappedArr[id];
    if (mappedElem.parent_category_id || mappedElem.parent_category_id === 0) {
      try {
        mappedArr[mappedElem.parent_category_id].children.push(mappedElem);
      } catch (e) {
        tree.push(mappedElem);
      }
    }
    // If the element is at the root level, add it to first level elements array.
    else {
      tree.push(mappedElem);
    }
  }
  return tree;
};
