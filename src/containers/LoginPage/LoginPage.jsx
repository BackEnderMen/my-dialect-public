import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import { useIntl } from 'react-intl';
import * as Yup from 'yup';
import { useLocation } from 'react-router';
import styles from './LoginPage.module.scss';
import useUser from '../../hooks/useUser';
import Typography from '../../components/Typography';
import Input from '../../components/Input';
import Button from '../../components/Button';
import ErrorBase from '../../components/Error';
import agent from '../../agent';
import useTitle from '../../hooks/useTitle';

function useQuery() {
  return new URLSearchParams(useLocation().search);
}

export const LoginPage = () => {
  const query = useQuery();

  const [error, setError] = useState('');
  const [verificationStatus, setVerificationStatus] = useState('');

  const { formatMessage: f } = useIntl();
  const history = useHistory();
  const { authLogin } = useUser();
  useEffect(() => {
    const token = query.get('token');
    if (token) {
      agent.Auth.verify(token)
        .then((res) => {
          setVerificationStatus(f({ id: 'you-confirmed-email' }));
          console.log(res);
        })
        .catch((err) => {
          setVerificationStatus('');
          setError(f({ id: 'error-confirm-email' }));
          console.log(err);
        });
    }
  }, []);

  useTitle(f({ id: 'login-page-title' }));

  const onSubmit = (values, { setSubmitting }) => {
    setSubmitting(true);
    return authLogin(values.email, values.password).then((res) => {
      if (res) {
        history.push('/');
      } else {
        setError(f({ id: 'login-error' }));
      }
      setSubmitting(false);
    });
  };

  const LoginSchema = Yup.object().shape({
    email: Yup.string()
      .email(f({ id: 'invalid-email' }))
      .required(f({ id: 'required' })),

    password: Yup.string()
      .min(8, f({ id: 'field-min-chars' }, { count: 8 }))
      .required(f({ id: 'required' })),
  });

  return (
    <div className={styles.login_page}>
      <Typography variant="h2">{f({ id: 'login-page-title' })}</Typography>

      <div className={styles.login_form}>
        <Formik
          initialValues={{ email: '', password: '' }}
          validationSchema={LoginSchema}
          onSubmit={onSubmit}
          className={styles.form}
        >
          {({ isSubmitting, handleSubmit }) => (
            <Form>
              <Field
                type="email"
                name="email"
                label={f({ id: 'email' })}
                component={Input}
              />
              <ErrorMessage name="email" component={ErrorBase} />
              <Field
                name="password"
                label={f({ id: 'password' })}
                component={Input}
              />
              <ErrorMessage name="password" component={ErrorBase} />
              <Button
                loading={isSubmitting}
                onClick={handleSubmit}
                type="submit"
                value={f({ id: 'sign-in' })}
              />
              {verificationStatus && (
                <Typography variant="p">{verificationStatus}</Typography>
              )}
              <ErrorBase value={error} />
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
