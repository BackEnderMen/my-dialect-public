import React from 'react';
import styles from './Profile.module.scss';
import Typography from '../../components/Typography';
import logo from './test_logo.jpg';
import useUser from '../../hooks/useUser';
import Words from '../Words';

// DATA
const data = [
  {
    word: 'емелет',
    transcription: '[éмелет]',
  },
  {
    word: 'оболок',
    transcription: '[óболок]',
  },
  {
    word: 'оболок',
    transcription: '[óболок]',
  },
  {
    word: 'Альфатер',
    transcription: "[ал'фатер]",
  },
  {
    word: 'бімбов',
    transcription: "[б'íмбоў]",
  },

  {
    word: 'бірувати',
    transcription: "[б'ірувáти]",
  },
  {
    word: 'нагавіці',
    transcription: "[нагав'íц'і]",
  },
  {
    word: 'ловор',
    transcription: '[лóвор]',
  },
  {
    word: 'никай',
    transcription: '[никай]',
  },
  {
    word: 'Милай',
    transcription: '[милай]',
  },

  {
    word: 'лабошка',
    transcription: '[лáбошка]',
  },
  {
    word: 'крумплі',
    transcription: "[крумпл'і]",
  },
  {
    word: 'кочоня',
    transcription: "[коч'ô ́н'а]",
  },
  {
    word: 'кляганиць',
    transcription: "[кл'аґани ́ц']",
  },
  {
    word: 'ловор',
    transcription: '[ловор]',
  },

  {
    word: 'доган',
    transcription: '[догáн]',
  },
  {
    word: 'днись',
    transcription: "[днис']",
  },
  {
    word: 'дараб',
    transcription: '[дараб]',
  },
  {
    word: 'гутта',
    transcription: "[гут':]",
  },
  {
    word: 'грезно',
    transcription: '[грéзно]',
  },

  {
    word: 'гадковати',
    transcription: '[гаткôвáти]',
  },
  {
    word: 'гаті',
    transcription: "[ґáт'і]",
  },
  {
    word: 'вуйко',
    transcription: '[вýĭко]',
  },
  {
    word: 'верти',
    transcription: '[вéрти]',
  },
  {
    word: 'бітанга',
    transcription: "[б'ітáнґа]",
  },
];
const title = {
  text: 'Додані',
  word: 'слова',
};
// END

export const Profile = () => {
  const { user } = useUser();

  return (
    <>
      <div className={styles.profile_page}>
        <img src={logo} alt="Logo" />
        <div className={styles.user_info}>
          <Typography variant="h4">{`${user.name} ${user.surname}`}</Typography>
          <Typography variant="p">
            Інжинер, засновник компаній SpaceX & Tesla.
            Недавно хотів, щоб його ракета полетіла у космос.
            Оновлений вид діяльності. До цього працював у цирку.
          </Typography>
        </div>
      </div>
      <div className={styles.words}>
        <Words data={data} title={title} />
      </div>
    </>
  );
};
