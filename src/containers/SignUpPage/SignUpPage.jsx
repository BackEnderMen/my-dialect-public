import React from 'react';
import { useIntl } from 'react-intl';
import useTitle from '../../hooks/useTitle';

export const SignUpPage = () => {
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'sign-up-title' }));

  return (
    <div />
  );
};
