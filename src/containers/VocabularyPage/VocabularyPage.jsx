import React from 'react';
import { useIntl } from 'react-intl';
import useTitle from '../../hooks/useTitle';

export const VocabularyPage = () => {
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'vocabulary-page-title' }));

  return (
    <div />
  );
};
