import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useIntl } from 'react-intl';
import useUser from '../../hooks/useUser';
import ErrorBase from '../../components/Error';

export const LogOut = () => {
  const history = useHistory();
  const { authLogout } = useUser();
  const [error, setError] = useState('error007');
  const { formatMessage: f } = useIntl();

  const onExit = () => authLogout().then((res) => {
    if (res) {
      history.push('/sign-up');
      window.location.reload();
      localStorage.removeItem('token');
    } else {
      setError(f({ id: 'logout-error' }));
    }
  });
  return (
    <>
      {onExit()}
      <ErrorBase value={error} />
    </>
  );
};
