import React from 'react';
import { NavLink } from 'react-router-dom';

import styles from './Header.module.scss';

export const Header = () => {
  const token = localStorage.getItem('token');

  return (
    <div className={styles.nav}>
      <NavLink exact to="/" activeClassName={styles.active}>
        Головна
      </NavLink>
      <NavLink exact to="/articles" activeClassName={styles.active}>
        Статті
      </NavLink>
      <NavLink exact to="/map" activeClassName={styles.active}>
        Карта
      </NavLink>
      <NavLink exact to="/vocabulary" activeClassName={styles.active}>
        Словник
      </NavLink>
      {token ? (
        <>
          <NavLink exact to="/profile" activeClassName={styles.active}>
            Профіль
          </NavLink>
          <NavLink exact to="/log-out" activeClassName={styles.active}>
            Вийти
          </NavLink>
        </>
      ) : (
        <>
          <NavLink exact to="/login" activeClassName={styles.active}>
            Вхід
          </NavLink>
          <NavLink exact to="/sign-up" activeClassName={styles.active}>
            Реєстрація
          </NavLink>
        </>
      )}
    </div>
  );
};
