import React, { useEffect } from 'react';
import './style.scss';
import { useIntl } from 'react-intl';
import AboutUs from '../AboutUs';
import SubNav from '../SubNav';
import Words from '../Words';
import useUser from '../../hooks/useUser';
import useTitle from '../../hooks/useTitle';

// DATA
const data = [
  {
    word: 'емелет',
    transcription: '[éмелет]',
  },
  {
    word: 'оболок',
    transcription: '[óболок]',
  },
  {
    word: 'оболок',
    transcription: '[óболок]',
  },
  {
    word: 'Альфатер',
    transcription: "[ал'фатер]",
  },
  {
    word: 'бімбов',
    transcription: "[б'íмбоў]",
  },

  {
    word: 'бірувати',
    transcription: "[б'ірувáти]",
  },
  {
    word: 'нагавіці',
    transcription: "[нагав'íц'і]",
  },
  {
    word: 'ловор',
    transcription: '[лóвор]',
  },
  {
    word: 'никай',
    transcription: '[никай]',
  },
  {
    word: 'Милай',
    transcription: '[милай]',
  },

  {
    word: 'лабошка',
    transcription: '[лáбошка]',
  }, {
    word: 'крумплі',
    transcription: "[крумпл'і]",
  }, {
    word: 'кочоня',
    transcription: "[коч'ô ́н'а]",
  }, {
    word: 'кляганиць',
    transcription: "[кл'аґани ́ц']",
  }, {
    word: 'ловор',
    transcription: '[ловор]',
  },

  {
    word: 'доган',
    transcription: '[догáн]',
  }, {
    word: 'днись',
    transcription: "[днис']",
  }, {
    word: 'дараб',
    transcription: '[дараб]',
  }, {
    word: 'гутта',
    transcription: "[гут':]",
  }, {
    word: 'грезно',
    transcription: '[грéзно]',
  },

  {
    word: 'гадковати',
    transcription: '[гаткôвáти]',
  }, {
    word: 'гаті',
    transcription: "[ґáт'і]",
  }, {
    word: 'вуйко',
    transcription: '[вýĭко]',
  }, {
    word: 'верти',
    transcription: '[вéрти]',
  }, {
    word: 'бітанга',
    transcription: "[б'ітáнґа]",
  },

];
const title = {
  text: 'ОСТАННІ ДОДАНІ',
  word: 'CЛОВА',
};
// END

export const Home = () => {
  const { getUser } = useUser();
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'home-title' }));

  useEffect(() => {
    getUser();
  }, []);
  return (
    <div className="home">
      <SubNav />
      <AboutUs />
      <Words data={data} title={title} />
    </div>
  );
};
