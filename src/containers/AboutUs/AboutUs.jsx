import React from 'react';
import styles from './AboutUs.module.scss';

export const AboutUs = () => (
  <div className={styles.container}>
    <div className={styles.box}>
      <div className={styles.img} />
      <div className={styles.info}>
        <h3 className={styles.title}>
          ПРО
          <span className={styles.border}> ПРОЄКТ </span>
        </h3>

        <p className={styles.text}>
          Вiтаємо на сайтi iнтерактивної мапи дiалектiв української мови! Цей проєкт розроблений у
          спiвпрацi фахiвцiв фiлологiчного факультету та факультету iнформацiйних технологiй Ужгородського
          нацiонального унiверситету.
        </p>
        <p className={styles.text}>
          Наша мета - сприяти збереженню українських говорiв, полегшити їх вивчення
          й популяризувати результати дiалектологiчних студiй.
        </p>
      </div>
    </div>
  </div>
);
