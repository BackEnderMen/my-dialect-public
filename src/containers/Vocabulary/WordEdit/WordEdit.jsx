import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import styles from './WordEdit.module.scss';
import useWord from '../../../hooks/useWord';

export const WordEdit = () => {
  const { dialect_name, transcription } = useParams();

  const { word, getWord } = useWord();

  useEffect(() => {
    getWord(dialect_name, transcription);
  }, []);

  return <div className={styles.display}>{JSON.stringify(word, null, 2)}</div>;
};
