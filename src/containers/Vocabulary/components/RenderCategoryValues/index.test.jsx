import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import RenderCategoryValues from './index';

Enzyme.configure({ adapter: new Adapter() });

describe('<RenderCategoryValues />', () => {
  const handleRender = jest.fn();

  const testValues = {
    'dialect group': 'group',
    'part of speech': 'noun',
    source: 'wiki',
    confirm: 't',
  };

  const rootCategory = {
    parent_category_id: null,
    category_type: 'category',
    children: [
      {
        id: 2,
        locale: 'ua',
        parent_category_id: 1,
        name: 'dialect group',
        category_type: 'category',
      },
      {
        id: 3,
        locale: 'ua',
        parent_category_id: 1,
        name: 'part of speech',
        category_type: 'selector',
      },
    ],
  };

  describe('Search component', () => {
    it('renders', () => {
      const wrapper = shallow(<RenderCategoryValues />);

      expect(wrapper.exists()).toBe(true);
    });
  });


  describe('render root of tree', () => {
    it('should render root component', () => {
      const wrapper = shallow(<RenderCategoryValues category={rootCategory} values={testValues} />);

      expect(wrapper.find('thead')).toHaveLength(1);
    });

    let valueItem;
    beforeEach(() => {
      valueItem = shallow(<RenderCategoryValues category={rootCategory} values={testValues} />);
    });

    it('renders tr', () => {
      expect(valueItem.find('tr').length).toBe(1);
    });
  });

  describe('render category but not root of tree', () => {
    it('should render category but not root', () => {
      const wrapper = shallow(<RenderCategoryValues category={{}} values={[]} />);

      wrapper.setProps({
        category: {
          parent_category_id: 1,
          category_type: 'category',
          children: [
            {
              id: 3,
              locale: 'ua',
              parent_category_id: 1,
              name: 'dialect group',
              category_type: 'category',
            },
            {
              id: 4,
              locale: 'ua',
              parent_category_id: 1,
              name: 'part of speech',
              category_type: 'selector',
            },
          ],
        },
      });

      expect(wrapper.find('thead')).toHaveLength(0);
    });
  });


  describe('render checkbox true', () => {
    it('render checkbox true', () => {
      const wrapper = shallow(<RenderCategoryValues category={{}} values={testValues} />);

      wrapper.setProps({
        category: {
          parent_category_id: 1,
          category_type: 'checkbox',
          name: 'confirm',
          children: [
            {
              id: 3,
              locale: 'ua',
              parent_category_id: 1,
              name: 'dialect group',
              category_type: 'category',
            },
            {
              id: 4,
              locale: 'ua',
              parent_category_id: 1,
              name: 'part of speech',
              category_type: 'selector',
            },
          ],
        },
      });

      expect(wrapper.find('FontAwesomeIcon')).toHaveLength(1);
    });
    it('render checkbox false', () => {
      const wrapper = shallow(<RenderCategoryValues category={{}} values={testValues} />);

      wrapper.setProps({
        category: {
          parent_category_id: 1,
          category_type: 'checkbox',
          name: 'smth',
          children: [
            {
              id: 3,
              locale: 'ua',
              parent_category_id: 1,
              name: 'dialect group',
              category_type: 'category',
            },
            {
              id: 4,
              locale: 'ua',
              parent_category_id: 1,
              name: 'part of speech',
              category_type: 'selector',
            },
          ],
        },
      });

      expect(wrapper.find('FontAwesomeIcon')).toHaveLength(0);
    });
  });

  it('handleRender .toHaveBeenCalled()', () => {
    handleRender();
    expect(handleRender).toHaveBeenCalled();
  });

  it('handleRender .toHaveBeenCalledTimes()', () => {
    handleRender(rootCategory.children, testValues);
    expect(handleRender).toHaveBeenCalledTimes(2);
  });

  it('handleRender with 3-levels ', () => {
    const categoryItem = {
      parent_category_id: 1,
      category_type: 'category',
      children: [
        {
          id: 3,
          locale: 'ua',
          parent_category_id: 1,
          name: 'dialect group',
          category_type: 'category',
          children: [
            {
              id: 5,
              locale: 'ua',
              parent_category_id: 3,
              category_type: 'category',
              name: 'nested category',
            },
          ],
        },
        {
          id: 4,
          locale: 'ua',
          parent_category_id: 1,
          name: 'part of speech',
          category_type: 'selector',
        },
      ],
    };
    handleRender(categoryItem.children, testValues);

    expect(handleRender).toHaveBeenCalledTimes(3);
  });
});
