import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import styles from '../Components.module.scss';


export const RenderCategoryValues = ({ category, values }) => {
  const {
    category_type: type,
    name,
    parent_category_id,
    children,
  } = category;

  const handleRender = (childrenItems, valuesItems) => (
    childrenItems.map((child) => (<RenderCategoryValues category={child} values={valuesItems} />))
  );

  console.log(category);
  console.log(values);

  if (type === 'category') {
    return parent_category_id === null ? (
      <>
        <thead>
          <tr className={styles.category}>
            <th className={styles.mainCell}>{name}</th>
          </tr>
        </thead>
        <tbody>
          {children?.length < 1 && (
            <tr>
              <th>Немає значень</th>
            </tr>
          )}
          {handleRender(children, values)}
        </tbody>
      </>
    ) : (
      <>
        <tr><th className={styles.mainCell}>{name}</th></tr>
        {children?.length < 1 && (
          <tr>
            <th>Немає значень</th>
          </tr>
        )}
        {type !== 'category' ? (
          <tr>
            <th className={styles.categoryValue}>
              {(values === undefined || typeof values[name] === 'undefined')
                ? '-'
                : values[name]}
            </th>
          </tr>
        )
          : null}
        {handleRender(children, values)}
      </>
    );
  } if (type === 'checkbox') {
    return (
      <tbody>
        <tr>
          <th>{name}</th>
          <th className={styles.categoryValue}>
            {values === undefined
            || typeof values[name] === 'undefined'
            || values[name] !== 't' ? (
                '-'
              ) : (
                <FontAwesomeIcon icon={faCheck} />
              )}
          </th>
        </tr>
      </tbody>
    );
  }
  if (type === 'textarea') {
    return (
      <tbody>
        <tr>
          <th>{name}</th>

          <th className={styles.categoryValue}>
            {(values === undefined || typeof values[name] === 'undefined') && type !== 'category'
              ? '-'
              : (<span dangerouslySetInnerHTML={{ __html: values[name] }} />)}
          </th>
        </tr>
      </tbody>
    );
  }
  return (
    <tbody>
      <tr>
        <th>{name}</th>

        <th className={styles.categoryValue}>
          {(values === undefined || typeof values[name] === 'undefined') && type !== 'category'
            ? '-'
            : values[name]}
        </th>
      </tr>
    </tbody>
  );
};

RenderCategoryValues.propTypes = {
  category: PropTypes.exact({
    children: PropTypes.object,
    parent_category_id: PropTypes.number,
    name: PropTypes.string,
  }),
  values: PropTypes.object,
};

RenderCategoryValues.defaultProps = {
  category: PropTypes.exact({
    children: {},
    parent_category_id: null,
    name: '',
  }),
  values: {},
};
