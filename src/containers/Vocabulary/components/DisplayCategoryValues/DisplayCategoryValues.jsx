import React, { useEffect, useState } from 'react';
import Skeleton from 'react-loading-skeleton';
import PropTypes from 'prop-types';
import styles from '../Components.module.scss';
import useCategory from '../../../../hooks/useCategory';
import { unflutten } from '../../../../helpers/unflutten';
import RenderCategoryValues from '../RenderCategoryValues';

export const DisplayCategoryValues = ({ locale, values }) => {
  const { getCategoryByLocale, categories } = useCategory();
  const [loading, setLoading] = useState(true);
  const [tree, setTree] = useState(null);

  useEffect(() => {
    setLoading(true);
    getCategoryByLocale(locale).finally(() => setLoading(false));
  }, []);

  useEffect(() => {
    setTree(unflutten(categories));
  }, [categories]);

  return loading ? (
    <div className={styles.skeleton}>
      <Skeleton height={34} className={styles.skeletonItem} />
      <Skeleton count={5} />
    </div>
  ) : (
    <div className={styles.displayCategories}>
      {tree.map((t) => (
        <table className={styles.categoriesTable}>
          <RenderCategoryValues category={t} values={values} />
        </table>
      ))}
    </div>
  );
};

DisplayCategoryValues.propTypes = {
  locale: PropTypes.string,
  values: PropTypes.object,
};

DisplayCategoryValues.defaultProps = {
  locale: 'en',
  values: {},
};
