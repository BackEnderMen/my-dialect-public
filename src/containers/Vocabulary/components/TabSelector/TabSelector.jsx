// eslint-disable jsx-a11y/no-static-element-interactions

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  faAngleRight,
  faAngleLeft,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../Components.module.scss';
import classNames from '../../../../helpers/classNames';


export const TabSelector = ({ handleSelect, list, selected }) => {
  const scrollRef = useRef();
  const [rightArrowVisible, setRightArrowVisible] = useState(true);
  const [leftArrowVisible, setLeftArrowVisible] = useState(false);

  const scrollHandler = () => {
    if (scrollRef.current.scrollLeft > 50) {
      setLeftArrowVisible(true);
    } else {
      setLeftArrowVisible(false);
    }
    if (scrollRef.current.scrollLeft > scrollRef.current.scrollWidth - scrollRef.current.clientWidth - 50) {
      setRightArrowVisible(false);
    } else {
      setRightArrowVisible(true);
    }
  };

  const handleRightArrow = () => {
    scrollRef.current.scrollTo({
      top: 0,
      left: scrollRef.current.scrollLeft + 500,
      behavior: 'smooth',
    });
  };
  const handleLeftArrow = () => {
    scrollRef.current.scrollTo({
      top: 0,
      left: scrollRef.current.scrollLeft - 500,
      behavior: 'smooth',
    });
  };

  useEffect(() => {
    scrollRef.current.addEventListener('scroll', scrollHandler);
    return () => scrollRef.current.removeEventListener('scroll', scrollHandler);
  });

  return (
    <div className={styles.tabSelector}>
      <FontAwesomeIcon
        className={classNames(styles.icon, leftArrowVisible ? styles.visible : '')}
        icon={faAngleLeft}
        onClick={handleLeftArrow}
      />
      <div className={styles.tabsWrapper} ref={scrollRef}>
        {
      list.map((elem, i) => (
        <button
          className={classNames(styles.tabItem, selected?.id === elem?.id ? styles.seletedItem : '')}
          type="button"
          key={elem?.id || i}
          onClick={() => handleSelect(elem)}
        >
          {elem.name}
        </button>
      ))
    }
      </div>
      <FontAwesomeIcon
        className={classNames(styles.icon, rightArrowVisible ? styles.visible : '')}
        icon={faAngleRight}
        onClick={handleRightArrow}
      />

    </div>

  );
};

TabSelector.propTypes = {
  handleSelect: PropTypes.func,
  list: PropTypes.array,
  selected: PropTypes.object,
};

TabSelector.defaultProps = {
  handleSelect: (item) => item,
  list: [],
  selected: null,
};
