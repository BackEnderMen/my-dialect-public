import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useIntl } from 'react-intl';
import styles from './Word.module.scss';
import useWord from '../../../hooks/useWord';
import Typography from '../../../components/Typography';
import { TabSelector } from '../components/TabSelector/TabSelector';
import { DisplayCategoryValues } from '../components/DisplayCategoryValues/DisplayCategoryValues';

export const Word = () => {
  const { dialect_name, transcription } = useParams();
  const { formatMessage: f } = useIntl();
  const generalInfo = { id: -1, name: f({ id: 'default-tab' }) };
  const { word, getWord } = useWord();
  const [selectedItem, setSelected] = useState(generalInfo);
  const categoryValues = selectedItem.id === -1 ? word?.category_values : selectedItem?.category_values;

  const tabsList = [generalInfo,
    ...(word?.cities ? word.cities.filter((c) => c.name).sort((a, b) => {
      if (a.name < b.name) { return -1; }
      if (a.name > b.name) { return 1; }
      return 0;
    }) : [])];

  const handleSelect = (item) => {
    setSelected(item);
  };

  useEffect(() => {
    getWord(dialect_name, transcription);
  }, []);

  return (
    <div
      className={styles.wordPage}
    >
      <div className="header">
        <Typography variant="h3" className={styles.subtitle}>
          {word?.official_word?.name}
        </Typography>
        <Typography variant="h1" className={styles.title}>
          {word?.name}
          {' '}
          {word?.transcription}
        </Typography>
      </div>
      <TabSelector
        list={tabsList}
        selected={selectedItem}
        handleSelect={handleSelect}
      />
      <div className="tabDetails">
        <Typography variant="h4">{selectedItem.name}</Typography>
        {selectedItem.region_name
          && (
          <Typography variant="p">
            {`${selectedItem.region_name} ${f({ id: 'region-abbr' })}, 
            ${selectedItem.district_name} ${f({ id: 'district-abbr' })}`}
          </Typography>
          )}
      </div>
      <DisplayCategoryValues locale={word?.locale || 'ua'} values={categoryValues} />

    </div>
  );
};
