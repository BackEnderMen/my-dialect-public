import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import Map from '../../components/Map';
import Input from '../../components/Input';
import Button from '../../components/Button';
import styles from './style.module.scss';
import CheckBox from '../../components/CheckBox';
import useWord from '../../hooks/useWord';
import usePlace from '../../hooks/usePlace';
import useTitle from '../../hooks/useTitle';
import AdvancedEditor from '../AdvancedEditor';

export const MapPage = () => {
  const [search, setSearch] = useState('');
  const [advancedEditor, toggleAdvancedEditor] = useState(false);
  const {
    dialectWords, getDialectWords, selectDialectWord, selectedWord,
  } = useWord();
  const { cities } = usePlace();
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'map-page-title' }));

  return (
    <div className={styles.page}>
      <div
        className={[
          styles.mapWrapper,
          advancedEditor ? styles.withEditor : '',
        ].join(' ')}
      >
        <div className={styles.searchingRow}>
          <Input
            classNames={{ label: styles.searchingInput }}
            placeholder="write a word"
            value={search}
            onChange={(text) => setSearch(text)}
          />
          <Button
            value="Search"
            variant="secondary"
            size="small"
            onClick={() => getDialectWords(search)}
          />
        </div>
        <div className={styles.checkbox}>
          <CheckBox
            value={advancedEditor}
            onChange={(newValue) => toggleAdvancedEditor(newValue)}
            label="Продвинуте редагування"
          />
        </div>
        <Map
          words={dialectWords}
          cities={cities}
          selectDialectWord={selectDialectWord}
          word={selectedWord}
        />
      </div>
      {advancedEditor && (
        <AdvancedEditor handleClose={() => toggleAdvancedEditor(false)} />
      )}
    </div>
  );
};
