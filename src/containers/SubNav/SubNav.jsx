import React from 'react';
import styles from './SubNav.module.scss';

export const SubNav = () => (
  <div className={styles.SubNav}>
    <div className={styles.grid}>
      <div className={styles.block}>
        <span />
        <span className={styles.logo_text}>Д</span>
        <span>I</span>
        <span>А</span>
      </div>
      <div className={styles.block}>

        <span className={styles.box1}>
          ІНТЕРАКТИВНА
          <br />
          {' '}
          КАРТА ДІАЛЕКТІВ
          <br />
          {' '}
          УКРАЇНИ
        </span>
        <span className={styles.center}>Л</span>
        <span className={styles.center}>Е</span>
      </div>
      <div className={styles.block}>

        <span className={styles.center}>К</span>
        <span />
        <span>Т</span>
        <span>И</span>
      </div>
      <div><span className={styles.text}>УКРАЇНИ</span></div>
    </div>

    <div className={styles.btn_scrollDown} />
  </div>
);
