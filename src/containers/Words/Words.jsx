/* eslint-disable react/destructuring-assignment, react/prop-types, react/no-array-index-key */

import React from 'react';
import styles from './Words.module.scss';

export const Words = (props) => {
  const arr = props.data
    .map((e, i) => (
      <a className={styles.elem} key={i} href="/">
        {e.word}
        {' '}
        {e.transcription}
      </a>
    ));

  return (
    <div className={styles.words}>
      <h2 className={styles.title}>
        {props.title.text}
        {' '}
        <span className="title--border">{props.title.word}</span>
      </h2>
      <div className={styles.gridBox}>
        {arr}
      </div>
    </div>
  );
};
