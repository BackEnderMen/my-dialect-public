import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import styles from './StyleSheet.module.scss';
import Input from '../../components/Input';
import Button from '../../components/Button';
import Typography from '../../components/Typography';
import CheckBox from '../../components/CheckBox';
import Audio from '../../components/Audio';
import AudioExample from './test.mp3';
import { TextArea } from '../../components/TextArea/TextArea';
import { Dropdown } from '../../components/Dropdown/Dropdown';
import Popup from '../../components/Popup';
import MediaButton from '../../components/MediaButton';
import useTitle from '../../hooks/useTitle';

export const StyleSheet = () => {
  const [value, setValue] = useState('');
  const [loading] = useState(false);
  const [selected, setSelected] = useState('');
  const [checkBoxValue, setCheckBoxValue] = useState(false);
  const [visible, setVisible] = useState(false);
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'style-sheet-title' }));

  console.log(visible);
  return (
    <div className={styles.sheet}>
      <Popup
        onSubmit={() => setVisible(false)}
        visible={visible}
        onExit={() => setVisible(false)}
      >
        {' '}
        <h1>Title</h1>
        <p>
          TestText TestText TestText TestText TestText
          TestText TestText TestText TestText TestText
        </p>
      </Popup>

      <h1 className={styles.title}>StyleSheet</h1>

      <h2 className={styles.subtitle}>Inputs</h2>
      <div className={styles.row}>
        <Input
          label="Ведіть слово"
          placeholder="наприклад,альфатер"
          value={value}
          onChange={(text) => setValue(text)}
        />
        <Input
          label="Custom style"
          value={value}
          classNames={{ input: styles.input }}
          onChange={(text) => setValue(text)}
        />
      </div>

      <TextArea
        label="Title Markdown"
        onChange={(text) => setValue(text)}
        value={value}
        classNames={{
          textarea: styles.textarea,
          markdown: styles.markdown,
        }}
      />

      <Audio src={AudioExample} />

      <div className={styles.row}>
        <Dropdown
          onClick={(t) => setSelected(t)}
          title="Слово"
          data={['Вуйко', 'Штрімфлі', 'Сесе']}
          multiple={false}
        />

        <Dropdown
          onClick={(t) => {
            setSelected(t.join(' · '));
          }}
          title="Слово"
          data={['Вуйко', 'Штрімфлі', 'Сесе']}
          multiple
        />

        <Typography variant="p">{selected}</Typography>

        <h2 className={styles.subtitle}>Buttons</h2>
        <div className={styles.row}>
          <Button value="Large" variant="black" size="large" />
          <Button value="Medium" variant="secondary" size="medium" />
          <Button value="Small" variant="primary" size="small" />
        </div>
        <div className={styles.row}>
          <Button value="Large" variant="primary" size="large" loading />
          <Button value="Large" variant="primary" size="large" loading />
          <Button value="Medium" variant="secondary" size="medium" loading />
          <Button value="Small" variant="black" size="small" loading />
          <Button value="Small" variant="primary" size="small" loading />
        </div>
        <div className={styles.row}>
          <Button
            onClick={() => setVisible(true)}
            value="Click me!"
            variant="primary"
            size="large"
            loading={loading}
          />
          <Button
            className={styles.customButton}
            value="Custom Style 🚀"
            variant="secondary"
            size="medium"
          />
        </div>
        <Typography variant="p">{selected}</Typography>

        <h2 className={styles.subtitle}>Buttons</h2>
        <div className={styles.row}>
          <Button value="Large" variant="primary" size="large" />
          <Button value="Medium" variant="secondary" size="medium" />
          <Button value="Small" variant="primary" size="small" />
        </div>
        <div className={styles.row}>
          <Button value="Large" variant="primary" size="large" loading />
          <Button value="Medium" variant="secondary" size="medium" loading />
          <Button value="Small" variant="primary" size="small" loading />
        </div>
        <div className={styles.row}>
          <Button
            onClick={() => setVisible(true)}
            value="Click me!"
            variant="primary"
            size="large"
            loading={loading}
          />
          <Button
            className={styles.customButton}
            value="Custom Style 🚀"
            variant="secondary"
            size="medium"
          />
        </div>

        <Typography variant="h1">H1 / Roboto</Typography>
        <Typography variant="h2">H2 / Roboto</Typography>
        <Typography variant="h3">H3 / Roboto</Typography>
        <Typography variant="h4">H4 / Roboto</Typography>
        <Typography variant="h5">H5 / Roboto</Typography>
        <Typography variant="h6">H6 / Roboto</Typography>
        <Typography variant="p">P / Roboto</Typography>

        <CheckBox
          value={checkBoxValue}
          onChange={(newValue) => {
            console.log(newValue);
            setCheckBoxValue(newValue);
          }}
          label="Title text"
        />
      </div>

      <MediaButton mediaType=".pdf" label="lable" />
    </div>
  );
};
