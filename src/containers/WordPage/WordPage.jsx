import React from 'react';
import { useIntl } from 'react-intl';
import useTitle from '../../hooks/useTitle';

export const WordPage = () => {
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'word-page-title' }));

  return (
    <div />
  );
};
