import React, { useState, useEffect } from 'react';
import {
  faObjectGroup,
  faCaretSquareDown,
  faPenSquare,
  faPhotoVideo,
  faCheckSquare,
  faQuoteRight,
} from '@fortawesome/free-solid-svg-icons';
import styles from './CategoryBuilder.module.scss';
import useCategory from '../../hooks/useCategory';
import { Builder } from './components/Builder';
import { Controller } from './components/Controller';

export const CategoryBuilder = () => {
  const [locale, setLocale] = useState('ua');
  const { categories, getCategoryByLocale, addCategory } = useCategory();
  const [selectedItem, selectItem] = useState(null);

  const menu = [
    { icon: faObjectGroup, name: 'category', title: 'Category' },
    { icon: faPenSquare, name: 'input', title: 'Input' },
    { icon: faQuoteRight, name: 'textarea', title: 'Textarea' },
    { icon: faCaretSquareDown, name: 'selector', title: 'Selector' },
    { icon: faPhotoVideo, name: 'media', title: 'Media' },
    { icon: faCheckSquare, name: 'checkbox', title: 'Checkbox' },
  ];

  const onSelect = (e) => {
    setLocale(e.target.value);
  };

  const selectNewItem = (item) => {
    if (selectedItem?.id === item?.id) {
      selectItem(null);
    } else {
      selectItem(item);
    }
  };

  useEffect(() => {
    getCategoryByLocale(locale);
  }, [locale]);

  const addLayer = async (data) => {
    if (!categories.some((c) => !c.name)) {
      return addCategory({
        ...data,
        parent_category_id: selectedItem?.id,
        locale,
      });
    }

    return null;
  };

  return (
    <div>
      <select defaultValue="ua" onChange={onSelect}>
        <option value="ua">Українська</option>
        <option value="hu">Magyar</option>
      </select>

      {locale && (
        <div className={styles.wrapper}>
          <div className={styles.menu}>
            <Controller
              addLayer={addLayer}
              menu={menu}
              selectItem={selectItem}
              selectedItem={selectedItem}
            />
          </div>
          <div className={styles.content}>
            <Builder
              categories={categories}
              locale={locale}
              menu={menu}
              selectItem={selectNewItem}
              selectedItem={selectedItem}
              newLayer={null}
            />
          </div>
        </div>
      )}
    </div>
  );
};
