/* eslint-disable react/prop-types, no-shadow,
jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions, react/no-array-index-key */

import React, { useState } from 'react';
import styles from '../CategoryBuilder.module.scss';

import Input from '../../../components/Input';

export const DropDownEditor = ({ params = {}, onChange = () => {} }) => {
  const { title = '', values = [''] } = params;

  const [value, setValue] = useState('');

  const setTitle = (value) => {
    onChange({ ...params, name: value });
  };

  const addValue = () => {
    onChange({ ...params, default_values: [...values, value] });
    setValue('');
  };

  const onDelete = (value) => {
    onChange({ ...params, default_values: values.filter((v) => v !== value) });
  };

  return (
    <div className={styles.dropDown}>
      <Input
        placeholder="title"
        classNames={{ root: styles.noMargin }}
        value={title}
        onChange={setTitle}
      />
      <br />
      <div className={styles.related}>
        <Input
          placeholder="add value"
          classNames={{ root: styles.noMargin }}
          onKeyPress={(e) => (e.charCode === 13 ? addValue() : null)}
          value={value}
          onChange={(v) => setValue(v)}
        />
        {' '}
        <span className={styles.delCategory_absolute} onClick={addValue}>
          +
        </span>
        <ol>
          {values.map((v, i) => (
            <li key={i}>
              {v}
              {' '}
              <span className={styles.delCategory} onClick={() => onDelete(v)}>
                {'\u2A2F'}
              </span>
            </li>
          ))}
        </ol>
      </div>
    </div>
  );
};
