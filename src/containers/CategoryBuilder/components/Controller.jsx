/* eslint-disable  react/prop-types, jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */

import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from '../CategoryBuilder.module.scss';


export const Controller = ({ addLayer, menu }) => (
  <div>
    <div className={styles.menuList}>
      {menu.map((el) => (
        <span
          className={styles.menuElement}
          key={el?.name}
          onClick={() => {
            addLayer({ category_type: el.name, default_values: null, name: '' });
          }}
        >
          <span className={styles.elementContent}>
            <FontAwesomeIcon className={styles.menuIcon} icon={el.icon} />
            {el.title}
          </span>
        </span>
      ))}
    </div>
  </div>
);
