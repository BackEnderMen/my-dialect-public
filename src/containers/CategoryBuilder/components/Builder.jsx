/* eslint-disable  react/prop-types, jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */

import React, { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPen,
  faTimes,
  faSave,
  faPlus,
} from '@fortawesome/free-solid-svg-icons';
import styles from '../CategoryBuilder.module.scss';
import Typography from '../../../components/Typography';
import { unflutten } from '../../../helpers/unflutten';
import useCategory from '../../../hooks/useCategory';
import Input from '../../../components/Input';

export const Builder = ({
  categories,
  menu,
  selectItem,
  selectedItem,
}) => {
  const [tree, setTree] = useState();
  const { removeCategory, updateCategory } = useCategory();
  const [isEdit, setEdit] = useState(null);
  const [editValue, setEditValue] = useState(null);
  const [defaultValues, editDefaultValues] = useState({ value: '', array: [] });

  useEffect(() => {
    const item = categories.find((c) => c.id === isEdit);
    setEditValue(item?.name);
    if (item?.default_values) editDefaultValues({ value: '', array: item?.default_values });
  }, [isEdit]);

  useEffect(() => {
    setTree(unflutten(categories));
    const unEdited = categories.find((f) => !f.name);
    if (unEdited) {
      setEdit(unEdited.id);
    }
    // selectItem(categories[0]);
  }, [categories]);

  const onDelete = (item) => removeCategory(item.id);

  const handleUpdate = async (item) => updateCategory(item?.id, {
    name: editValue,
    default_values: defaultValues.array,
  }).finally(() => {
    setEditValue(null);
    setEdit(null);
    editDefaultValues([]);
  });

  const renderDefaultValues = (item) => {
    if (isEdit === item?.id) {
      return (
        <div className={styles.defaultValues_edit}>
          <span className={styles.editValue}>
            <Input
              value={defaultValues?.value}
              placeholder="Додайте значення за замовчуванням..."
              onChange={(value) => editDefaultValues({ ...defaultValues, value })}
            />
            <FontAwesomeIcon
              className={styles.plusIcon}
              icon={faPlus}
              onClick={() => editDefaultValues({
                ...defaultValues,
                array: [...defaultValues.array, defaultValues.value],
              })}
            />
          </span>
          {defaultValues?.array?.map((a, i, _element) => (
            <span className={styles.editValue} key={i}>
              {a}
              <FontAwesomeIcon
                className={styles.activeIcon}
                icon={faTimes}
                onClick={() => editDefaultValues({
                  ...defaultValues,
                  array: defaultValues?.array?.filter((v) => v !== a),
                })}
              />
            </span>
          ))}
        </div>
      );
    }
    return (
      <div className={styles.defaultValues}>
        {item?.default_values?.join(' · ')}
      </div>
    );
  };

  const renderElement = (item, primaryColor = true) => {
    const builderElement = [
      styles.builderElement,
      !primaryColor ? styles.builderElement_invert : '',
      selectedItem?.id === item?.id ? styles.selected : '',
    ].join(' ');

    const title = [styles.title, !primaryColor ? styles.title_invert : ''].join(
      ' ',
    );

    const el = menu.find((m) => m.name === item?.category_type);

    return (
      <div
        className={builderElement}
        key={item?.id}
        onClick={(e) => {
          e.stopPropagation();
          item?.category_type === 'category' && selectItem(item);
        }}
      >
        <div className={styles.titleBlock}>
          <FontAwesomeIcon className={styles.menuIcon} icon={el?.icon || ''} />
          {isEdit === item?.id ? (
            <Input
              placeholder="Назва блоку"
              value={editValue || ''}
              classNames={{ root: styles.input }}
              onChange={(v) => setEditValue(v)}
            />
          ) : (
            <Typography variant="h4" className={title}>
              {item?.name}
            </Typography>
          )}
          {isEdit === item?.id ? (
            <FontAwesomeIcon
              className={styles.activeIcon}
              icon={faSave}
              onClick={() => handleUpdate(item)}
            />
          ) : (
            <FontAwesomeIcon
              className={styles.activeIcon_edit}
              icon={faPen}
              onClick={() => setEdit(item?.id)}
            />
          )}

          <FontAwesomeIcon
            className={styles.activeIcon}
            icon={faTimes}
            onClick={() => (isEdit ? setEdit(null) : onDelete(item))}
          />
        </div>
        {item?.default_values && renderDefaultValues(item)}
        <div className={styles.children}>
          {item?.children?.map((ch) => renderElement(ch, !primaryColor))}
        </div>
      </div>
    );
  };

  return <div>{tree?.map((t) => renderElement(t)) }</div>;
};
