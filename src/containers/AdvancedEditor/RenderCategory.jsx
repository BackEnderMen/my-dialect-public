import React from 'react';
import PropTypes from 'prop-types';
import Typography from '../../components/Typography';
import styles from './AdvancedEditor.module.scss';
import MediaButton from '../../components/MediaButton';

import SecondaryDropdown from './components/SecondaryDropdown';
import SecondaryCheckBox from './components/SecondaryCheckBox';
import SecondaryInput from './components/SecondaryInput';
import SecondaryTextArea from './components/SecondaryTextArea';

export const RenderCategory = ({
  category,
  depth,
  value,
  handleField,
  values,
}) => {
  const {
    id, category_type: type, name, default_values, children,
  } = category;

  switch (type) {
    case 'category': {
      return (
        <div className={depth === 0 && styles.category}>
          <Typography
            variant={`h${depth < 2 ? depth + 2 : 5}`}
            className={styles[`title${depth < 2 ? depth + 1 : 3}`]}
          >
            {name}
          </Typography>
          {children?.map((ch) => (
            <RenderCategory
              category={ch}
              key={ch.id}
              depth={++depth}
              handleField={handleField}
              value={values && values[ch.id] ? values[ch.id] : null}
            />
          ))}
        </div>
      );
    }
    case 'input': {
      return (
        <SecondaryInput
          label={name}
          classNames={{
            label: styles.whiteRegular,
            root: styles.input,
            input: styles.field,
          }}
          value={value}
          onChange={(text) => handleField(id, text)}
        />
      );
    }
    case 'selector': {
      return (
        <SecondaryDropdown
          data={default_values}
          label={name}
          onClick={(v) => handleField(id, v)}
          classNames={{
            label: styles.dropDown_label,
            arrow: styles.arrow,
            root: styles.dropDown,
          }}
        />
      );
    }
    case 'checkbox': {
      return (
        <SecondaryCheckBox
          className={styles.checkbox}
          label={name}
          value={value || default_values[0]}
          onChange={() => handleField(id, !value)}
        />
      );
    }
    case 'textarea': {
      return (
        <SecondaryTextArea
          label={name}
          onChange={(rawValue) => handleField(id, rawValue)}
        />
      );
    }
    case 'media': {
      return (
        <MediaButton
          label={name}
          variant="secondary"
          size="small"
          value="test"
        />
      );
    }
    default:
      return null;
  }
};

RenderCategory.propTypes = {
  category: PropTypes.object,
  depth: PropTypes.number,
  value: PropTypes.string,
  handleField: PropTypes.func,
  values: PropTypes.object,
};

RenderCategory.defaultProps = {
  category: {},
  depth: 0,
  value: '',
  handleField: () => {},
  values: {},
};
