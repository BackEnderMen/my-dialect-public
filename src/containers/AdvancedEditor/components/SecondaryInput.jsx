import React from 'react';
import styles from '../AdvancedEditor.module.scss';
import Input from '../../../components/Input';

const SecondaryInput = (props) => (
  <Input
    classNames={{
      label: styles.whiteRegular,
      root: styles.input,
      input: styles.field,
    }}
    {...props}
  />
);

export default SecondaryInput;
