import React from 'react';
import styles from '../AdvancedEditor.module.scss';

export const SubmitButton = ({ children, ...props }) => (
  <button className={styles.submit} type="submit" {...props}>
    {children}
  </button>
);
