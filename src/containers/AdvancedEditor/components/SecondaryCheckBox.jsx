import React from 'react';
import styles from '../AdvancedEditor.module.scss';
import CheckBox from '../../../components/CheckBox';

const SecondaryCheckBox = (props) => <CheckBox className={styles.checkbox} {...props} />;

export default SecondaryCheckBox;
