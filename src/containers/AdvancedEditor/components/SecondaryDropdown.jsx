import React from 'react';
import styles from '../AdvancedEditor.module.scss';
import Dropdown from '../../../components/Dropdown';

const SecondaryDropdown = (props) => (
  <Dropdown
    classNames={{
      label: styles.dropDown_label,
      arrow: styles.arrow,
      root: styles.dropDown,
    }}
    {...props}
  />
);

export default SecondaryDropdown;
