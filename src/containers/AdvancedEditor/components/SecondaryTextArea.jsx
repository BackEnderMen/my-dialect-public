import React from 'react';
import styles from '../AdvancedEditor.module.scss';
import TextArea from '../../../components/TextArea';

const SecondaryTextArea = (props) => (
  <TextArea
    classNames={{
      root: styles.textArea_root,
      label: styles.textArea_label,
      markdown: styles.textArea_markdown,
      toolbar: styles.textArea_toolbar,
    }}
    {...props}
  />
);

export default SecondaryTextArea;
