import React, { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleRight } from '@fortawesome/free-solid-svg-icons';
import { omit } from 'lodash';
import { useIntl } from 'react-intl';
import PropTypes from 'prop-types';
import styles from './AdvancedEditor.module.scss';
import Typography from '../../components/Typography';
import useUser from '../../hooks/useUser';
import useCategory from '../../hooks/useCategory';
import useWord from '../../hooks/useWord';
import usePlace from '../../hooks/usePlace';
import { unflutten } from '../../helpers/unflutten';
import { RenderCategory } from './RenderCategory';

import SecondaryInput from './components/SecondaryInput';
import SecondaryDropdown from './components/SecondaryDropdown';
import { SubmitButton } from './components/SubmitButton';

export const AdvancedEditor = ({ handleClose }) => {
  const { getCategoryByLocale, categories } = useCategory();
  const {
    getCountries,
    countries,
    districts,
    regions,
    cities,
    selectRegion,
    selectCountry,
    selectDistrict,
  } = usePlace();
  const { createWord } = useWord();
  const { locale } = useUser();
  const [tree, setTree] = useState(null);
  const [values, setValues] = useState({});
  const [selectedRegion, selectNewRegion] = useState(null);
  const [, selectCities] = useState();
  const { formatMessage: f } = useIntl();

  const handleField = (id, value) => {
    setValues({ ...values, [id]: value });
  };

  useEffect(() => {
    getCountries();
  }, []);

  useEffect(() => {
    getCategoryByLocale(locale);
  }, [locale]);

  useEffect(() => {
    setTree(unflutten(categories));
  }, [categories]);

  const handleSelectCountry = (data) => selectCountry(data.id);

  const handleSelectDistrict = (data) => selectDistrict(data.id);

  const handleSelectRegion = (data) => {
    selectNewRegion(data.id);
    return selectRegion(data.id);
  };

  const handleSelectCity = (data) => {
    setValues({ ...values, city_ids: data.map((c) => c.id) });
    selectCities(data);
  };

  const handleSubmit = () => {
    const {
      official_word, transcription, dialect, city_ids,
    } = values;
    const dialect_category_values = {
      ...omit(values, [
        'official_word',
        'transcription',
        'dialect',
        'city_ids',
      ]),
    };
    const data = {
      dialect_name: dialect,
      dialect_transcription: transcription,
      official_name: official_word,
      dialect_category_values,
      city_ids,
    };
    return createWord({ word: data });
  };

  return (
    <div className={styles.editor}>
      <div className={styles.header}>
        <span />
        <Typography variant="h4" className={styles.title}>
          {f({ id: 'advanced-editor' })}
        </Typography>
        <FontAwesomeIcon
          icon={faAngleRight}
          className={styles.icon}
          onClick={handleClose}
        />
      </div>
      <div className={styles.body}>
        <SecondaryInput
          label={f({ id: 'dialect' })}
          value={values.dialect}
          onChange={(text) => handleField('dialect', text)}
          classNames={{
            label: styles.whiteRegular,
            root: styles.input,
            input: styles.field,
          }}
        />
        <SecondaryInput
          label={f({ id: 'transaction' })}
          value={values.transcription}
          onChange={(text) => handleField('transcription', text)}
          classNames={{
            label: styles.whiteRegular,
            root: styles.input,
            input: styles.field,
          }}
        />
        <SecondaryInput
          label={f({ id: 'official_word' })}
          value={values.official_word}
          onChange={(text) => handleField('official_word', text)}
          classNames={{
            label: styles.whiteRegular,
            root: styles.input,
            input: styles.field,
          }}
        />
        <SecondaryDropdown
          label={f({ id: 'select-country' })}
          data={Object.values(countries)}
          onClick={handleSelectCountry}
        />
        <SecondaryDropdown
          label={f({ id: 'select-district' })}
          data={districts}
          onClick={handleSelectDistrict}
        />
        <SecondaryDropdown
          label={f({ id: 'select-region' })}
          data={regions}
          onClick={handleSelectRegion}
        />
        <SecondaryDropdown
          label={f({ id: 'select-city' })}
          multiple
          data={Object.values(cities)
            .map((c) => ({ ...c }))
            .filter((c) => c.region_id === selectedRegion)}
          onClick={handleSelectCity}
        />
        {tree?.map((t) => (
          <RenderCategory
            category={t}
            key={t.id}
            handleField={handleField}
            value={values[t.id]}
            depth={0}
            values={values}
          />
        ))}
      </div>
      <SubmitButton variant="secondary" onClick={handleSubmit}>
        {f({ id: 'create' })}
      </SubmitButton>
    </div>
  );
};
AdvancedEditor.propTypes = {
  handleClose: PropTypes.func,
};

AdvancedEditor.defaultProps = {
  handleClose: () => {},
};
