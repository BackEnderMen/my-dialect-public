import React from 'react';
import { render } from '@testing-library/react';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { IntlProvider } from 'react-intl';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import SignUp from './index';

Enzyme.configure({ adapter: new Adapter() });

const storeFake = (state) => ({
  default: jest.fn(),
  subscribe: jest.fn(),
  dispatch: jest.fn(),
  getState: () => state,
});
const history = createBrowserHistory();

describe('<Sign-up />', () => {
  const store = storeFake({});
  it('snapshot', () => {
    const container = render(
      <Provider store={store}>
        <Router history={history}>
          <IntlProvider locale="en">
            <SignUp />
          </IntlProvider>
        </Router>
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
