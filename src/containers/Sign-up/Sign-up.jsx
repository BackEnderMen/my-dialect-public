import React, { useState } from 'react';
import {
  Formik, Form, Field, ErrorMessage,
} from 'formik';
import { useIntl } from 'react-intl';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import styles from './Sign-up.module.scss';
import Input from '../../components/Input';
import ErrorBase from '../../components/Error';
import Button from '../../components/Button';
import Typography from '../../components/Typography';
import useUser from '../../hooks/useUser';

export const SignUp = () => {
  const { formatMessage: f } = useIntl();
  const { authRegister } = useUser();
  const history = useHistory();
  const [error, setError] = useState('');
  const [isSignUp, setSignUp] = useState(false);
  const [emailState, setEmailState] = useState('example@gmail.com');

  const onSubmit = (values, { setSubmitting }) => {
    setEmailState(values.email);
    setSubmitting(true);
    return authRegister(
      values.email,
      values.password,
      values.submit_password,
      values.surname,
      values.name,
      values.date,
    ).then((res) => {
      if (res) {
        history.push('/sign-up');
        setSignUp(true);
      } else {
        setError(f({ id: 'sign-up-error' }));
      }
      setSubmitting(false);
    });
  };

  const RegistrationSchema = Yup.object().shape({
    surname: Yup.string().required(f({ id: 'required' })),

    name: Yup.string().required(f({ id: 'required' })),

    date: Yup.date().required(f({ id: 'required' })),

    email: Yup.string()
      .email(f({ id: 'invalid-email' }))
      .required(f({ id: 'required' })),

    password: Yup.string()
      .min(8, f({ id: 'field-min-chars' }, { count: 8 }))
      .required(f({ id: 'required' })),

    submit_password: Yup.string()
      .min(8, f({ id: 'field-min-chars' }, { count: 8 }))
      .oneOf([Yup.ref('password')], f({ id: 'passwords-match' }))
      .required(f({ id: 'required' })),
  });

  return (
    <div className={styles.registration_form}>
      <Typography variant="h2">{f({ id: 'sign-up-page-title' })}</Typography>
      <Formik
        initialValues={{
          surname: '',
          name: '',
          date: '2001-02-24',
          email: '',
          password: '',
          submit_password: '',
        }}
        validationSchema={RegistrationSchema}
        onSubmit={onSubmit}
      >
        {({ isSubmitting, handleSubmit }) => (!isSignUp ? (
          <Form>
            <Field
              type="surname"
              name="surname"
              label={f({ id: 'surname' })}
              component={Input}
            />
            <div className={styles.error}>
              <ErrorMessage name="surname" component={ErrorBase} />
            </div>
            <Field
              type="name"
              name="name"
              label={f({ id: 'name' })}
              component={Input}
            />
            <div className={styles.error}>
              <ErrorMessage name="name" component={ErrorBase} />
            </div>
            <Field
              type="date"
              name="date"
              label={f({ id: 'b-date' })}
              component={Input}
            />
            <div className={styles.error}>
              <ErrorMessage name="date" component={ErrorBase} />
            </div>
            <Field
              type="email"
              name="email"
              label={f({ id: 'email' })}
              component={Input}
            />
            <div className={styles.error}>
              <ErrorMessage name="email" component={ErrorBase} />
            </div>
            <Field
              name="password"
              label={f({ id: 'password' })}
              component={Input}
            />
            <div className={styles.error}>
              <ErrorMessage name="password" component={ErrorBase} />
            </div>
            <Field
              name="submit_password"
              label={f({ id: 'submit-password' })}
              component={Input}
            />
            <div className={styles.error}>
              <ErrorMessage name="submit_password" component={ErrorBase} />
            </div>
            <div className={styles.button}>
              <Button
                loading={isSubmitting}
                onClick={handleSubmit}
                type="submit"
                value={f({ id: 'sign-up' })}
              />
              <ErrorBase value={error} />
            </div>
          </Form>
        ) : (
          <div className={styles.registration_complete}>
            <div className={styles.text}>
              <Typography variant="h4">
                {f({ id: 'you-did-registration' })}
              </Typography>
            </div>
            <div className={styles.text}>
              <Typography variant="h5">
                {f({ id: 'you-have-submit' }, { email: emailState })}
              </Typography>
            </div>
          </div>
        ))}
      </Formik>
    </div>
  );
};
