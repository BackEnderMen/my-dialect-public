import React from 'react';
import { useIntl } from 'react-intl';
import useTitle from '../../hooks/useTitle';


export const ArticlesPage = () => {
  const { formatMessage: f } = useIntl();

  useTitle(f({ id: 'articles-page-title' }));

  return (
    <div />
  );
};
