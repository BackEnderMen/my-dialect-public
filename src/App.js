import React, { useEffect } from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import './App.scss';
import StyleSheet from './containers/StyleSheet';
import store from './store';
import useUser from './hooks/useUser';
import intlMessages from './intl';

import Header from './containers/Header';
import Home from './containers/Home';
import MapPage from './containers/MapPage';
import LoginPage from './containers/LoginPage';
import CategoryBuilder from './containers/CategoryBuilder';
import SignUp from './containers/Sign-up';
import LogOut from './containers/LogOut';
import Profile from './containers/Profile';
import WordEdit from './containers/Vocabulary/WordEdit';
import Word from './containers/Vocabulary/Word';

const App = () => {
  const { locale, getUser } = useUser();

  useEffect(() => {
    if (localStorage.getItem('token')) getUser().catch(console.warn);
  }, []);

  return (
    <IntlProvider locale={locale} messages={intlMessages(locale)}>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/map" component={MapPage} />
          <Route exact path="/login" component={LoginPage} />
          <Route path="/stylesheet" component={StyleSheet} />
          <Route path="/sign-up" component={SignUp} />
          <Route path="/log-out" component={LogOut} />
          <Route path="/profile" component={Profile} />
          <Route exact path="/sign-in" component={LoginPage} />
          <Route exact path="/category/builder" component={CategoryBuilder} />
          <Route path="/vocabulary/:dialect_name/:transcription/edit" component={WordEdit} />
          <Route path="/vocabulary/:dialect_name/:transcription" component={Word} />
        </Switch>
      </BrowserRouter>
    </IntlProvider>
  );
};

const AppProviders = () => (
  <StoreProvider store={store}>
    <App />
  </StoreProvider>
);

export default AppProviders;
