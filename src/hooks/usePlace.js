import { useSelector } from 'react-redux';
import {
  setCities,
  getCountries,
  selectCountry,
  selectRegion,
  selectDistrict,
  getDialectWordsByTranscrition,
} from '../store/actions';
import useAsyncDispatch from './useAsyncDispatch';

export default function usePlace() {
  const place = useSelector((state) => state.places);
  // const selectedCountry = useSelector((state) =>
  //   state.places.countries.find((c) => c.selected)
  // );
  const dispatch = useAsyncDispatch();

  return {
    ...place,
    // selectedCountry,
    setCities: () => dispatch(setCities()),
    getCountries: () => dispatch(getCountries()),
    selectCountry: (id) => dispatch(selectCountry(id)),
    selectDistrict: (id) => dispatch(selectDistrict(id)),
    selectRegion: (id) => dispatch(selectRegion(id)),
    getDialectWordsByTranscrition:
      (name, transcription) => dispatch(getDialectWordsByTranscrition(name, transcription)),
  };
}
