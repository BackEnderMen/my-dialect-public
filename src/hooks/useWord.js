import { useSelector } from 'react-redux';
import {
  getDialectWords,
  setCities,
  selectDialectWord,
  createWord, getWord,
  clearWord,
} from '../store/actions';
import useAsyncDispatch from './useAsyncDispatch';

export default function useUser() {
  const word = useSelector((state) => state.word);
  const dispatch = useAsyncDispatch();

  return {
    ...word,
    getDialectWords: (search) => dispatch(getDialectWords(search)).then((data) => {
      const { cities } = data.value;
      return dispatch(setCities(cities));
    }),
    /* eslint-disable-next-line no-shadow */
    selectDialectWord: (word) => dispatch(selectDialectWord(word)),
    createWord: (data) => dispatch(createWord(data)).then(() => dispatch(getDialectWords(data.word.dialect_name))),
    getWord: (name, transcription) => dispatch(getWord(name, transcription)),
    clearWord: () => dispatch(clearWord()),
  };
}
