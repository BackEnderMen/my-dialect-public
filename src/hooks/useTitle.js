import { useIntl } from 'react-intl';

export default function useTitle(pageTitle) {
  const { formatMessage: f } = useIntl();
  document.title = `${pageTitle} | ${f({ id: 'project-title' })}`;
}
