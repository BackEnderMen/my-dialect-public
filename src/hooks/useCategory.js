import { useSelector } from 'react-redux';
import {
  getCategoryByLocale, addCategory, removeCategory, updateCategory,
} from '../store/actions';
import useAsyncDispatch from './useAsyncDispatch';

export default function useCategory() {
  const category = useSelector((state) => state.category);
  const dispatch = useAsyncDispatch();

  return {
    ...category,
    getCategoryByLocale: (locale) => dispatch(getCategoryByLocale(locale)),
    addCategory: (data) => dispatch(addCategory(data)),
    removeCategory: (id) => dispatch(removeCategory(id)),
    updateCategory: (id, params) => dispatch(updateCategory(id, params)),
  };
}
