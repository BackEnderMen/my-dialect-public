import { useSelector } from 'react-redux';
import {
  authLogin, authLogout, authRegister, getUser,
} from '../store/actions';
import useAsyncDispatch from './useAsyncDispatch';

export default function useUser() {
  const user = useSelector((state) => state.user);
  const dispatch = useAsyncDispatch();

  return {
    ...user,
    authLogin: (email, password) => dispatch(authLogin(email, password)),
    authLogout: () => dispatch(authLogout()),
    getUser: () => dispatch(getUser()),
    authRegister: (email, password, submit_password, surname, name, date) => dispatch(
      authRegister(email, password, submit_password, surname, name, date),
    ),
  };
}
